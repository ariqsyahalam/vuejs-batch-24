import { MemberCard } from "./components/MemberCard.js";

new Vue({
  el: "#app",
  data: {
    url: "http://demo-api-vue.sanbercloud.com/",
    members: [],
    inputMember: {
      name: "",
      address: "",
      no_hp: "",
    },
    currentId: null,
    editState: false,
    uploadState: false,
  },
  components: {
    "member-card": MemberCard,
  },
  methods: {
    validation: function () {
      String.prototype.isNumber = function () {
        return /^\d+$/.test(this);
      };
      if (this.inputMember.name.length < 3) {
        alert("Nama minimal memiliki 3 karakter");
        return false;
      }
      if (this.inputMember.address.length < 5) {
        alert("Alamat minimal memiliki 5 karakter");
        return false;
      }
      if (this.inputMember.no_hp.length < 10) {
        alert("Nomor Handphone minimal memiliki 10 digit");
        return false;
      }
      if (!this.inputMember.no_hp.isNumber()) {
        alert("Nomor Handphone tidak boleh mengandung selain angka");
        return false;
      }
      return true;
    },
    clearForm: function () {
      this.inputMember.name = "";
      this.inputMember.address = "";
      this.inputMember.no_hp = "";
      this.currentId = null;
    },
    getMember: async function () {
      try {
        const { data } = await axios.get(`${this.url}api/member`);
        this.members = data.members;
      } catch (e) {
        console.log(e);
      }
    },
    addMember: async function () {
      try {
        if (!this.validation()) {
          return;
        }
        const { response } = await axios.post(
          `${this.url}api/member`,
          this.inputMember
        );
        this.clearForm();
        this.getMember();
        alert("Berhasil menambahkan Member baru");
      } catch (e) {
        console.log(e);
      }
    },
    editMember: function (m) {
      this.editState = true;

      this.currentId = m.id;
      this.inputMember = {
        name: m.name,
        address: m.address,
        no_hp: m.no_hp,
      };
    },
    cancel: function () {
      this.editState = false;

      this.currentId = 0;
      this.inputMember = {
        name: "",
        address: "",
        no_hp: "",
      };
    },
    deleteMember: async function (id) {
      try {
        const { response } = await axios.post(
          `${this.url}api/member/${id}?_method=DELETE`,
          this.inputMember
        );
        this.getMember();
        alert("Berhasil menghapus member");
      } catch (e) {
        console.log(e);
      }
    },
    updateMember: async function () {
      try {
        if (!this.validation()) {
          return;
        }
        if (!this.uploadState) {
          const { response } = await axios.post(
            `${this.url}api/member/${this.currentId}?_method=PUT`,
            this.inputMember
          );
          alert("Berhasil mengupdate member");
        } else {
          const photo_profile = this.$refs.pp.files[0];
          const formData = new FormData();

          formData.set("photo_profile", photo_profile);

          const { response } = await axios.post(
            `${this.url}api/member/${this.currentId}/upload-photo-profile`,
            formData
          );
          alert("Berhasil mengupload gambar");
        }
        this.clearForm();
        this.getMember();
        this.editState = false;
        this.uploadState = false;
      } catch (e) {
        console.log(e);
      }
    },
    uploadPhoto: function (m) {
      this.editMember(m);
      this.uploadState = true;
    },
  },
  created() {
    this.getMember();
  },
});
