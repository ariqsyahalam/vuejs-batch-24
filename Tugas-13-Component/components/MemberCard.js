export var MemberCard = {
  props: ["url", "m"],
  template: `
  <table border="1">
  <tr>
	<td style="padding: 16px">
		<img
		width="160px"
		:src="m.photo_profile ? url + m.photo_profile : 'https://dummyimage.com/16:9x1080'"
		/>
	</td>
	<td style="width: 100%; padding: 16px; min-width: 500px;">
		<pre><b>ID              : </b> {{m.id}}</pre>
		<pre><b>Nama            : </b> {{m.name}}</pre>
		<pre><b>Alamat          : </b> {{m.address}}</pre>
		<pre><b>Nomor Handphone : </b> {{m.no_hp}}</pre>
	</td>
	<td align="right" style="padding: 16px">
		<input
		type="button"
		value="Edit"
		@click="$emit('edit-member', m)"
		style="width: 100%;"
		/>
		<p></p>
		<input
		type="button"
		value="Hapus"
		@click="$emit('delete-member', m.id)"
		style="width: 100%"
		/>
		<p></p>
		<input
		type="button"
		value="Upload Gambar"
		@click="$emit('upload-photo', m)"
		style="width: 100%"
		/>
	</td>
	</tr>
	</table border>
            
        `,
};
