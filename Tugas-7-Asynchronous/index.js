// di index.js
var readBooks = require("./callback.js");

var books = [
  { name: "LOTR", timeSpent: 3000 },
  { name: "Fidas", timeSpent: 2000 },
  { name: "Kalkulus", timeSpent: 4000 },
  { name: "komik", timeSpent: 1000 },
];

// soal 1
const getBook = (time, index, queue) => {
  readBooks(time, books[index], function (remainingTime) {
    time = remainingTime;
    queue = queue - 1;

    if (queue > 0) {
      getBook(time, index + 1, queue);
    }
  });
};

getBook(10000, 0, books.length);
