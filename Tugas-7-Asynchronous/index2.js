var readBooksPromise = require("./promise.js");

var books = [
  { name: "LOTR", timeSpent: 3000 },
  { name: "Fidas", timeSpent: 2000 },
  { name: "Kalkulus", timeSpent: 4000 },
];

// soal 2
function getBook(time, index, queue) {
  readBooksPromise(time, books[index])
    .then(function (remainingTime) {
      time = remainingTime;
      queue = queue - 1;
      if (queue > 0) {
        getBook(time, index + 1, queue);
      }
    });
}

getBook(10000, 0, books.length);
