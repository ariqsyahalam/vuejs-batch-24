// soal 1
var nilai = 70;
var index;

if (nilai >= 85) index = "A";
else if (nilai >= 75) index = "B";
else if (nilai >= 65) index = "C";
else if (nilai >= 55) index = "D";
else if (nilai < 55) index = "E";

console.log(index);
console.log();

// soal 2
var tanggal = 26;
var bulan = 6;
var tahun = 2000;

var bulanstring;
switch (bulan) {
  case 1:
    bulanstring = "Januari";
    break;
  case 2:
    bulanstring = "Februari";
    break;
  case 3:
    bulanstring = "Maret";
    break;
  case 4:
    bulanstring = "April";
    break;
  case 5:
    bulanstring = "Mei";
    break;
  case 6:
    bulanstring = "Juni";
    break;
  case 7:
    bulanstring = "Juli";
    break;
  case 8:
    bulanstring = "Agustus";
    break;
  case 9:
    bulanstring = "September";
    break;
  case 10:
    bulanstring = "Oktober";
    break;
  case 11:
    bulanstring = "November";
    break;
  case 12:
    bulanstring = "Desember";
    break;
}

console.log(tanggal + " " + bulanstring + " " + tahun + "\n");

// soal 3
function segitiga(n) {
  for (var i = 1; i <= n; i++) {
    var output = "";
    for (var j = 0; j < i; j++) {
      output += "#";
    }
    console.log(output);
  }
  console.log();
}

segitiga(3);
segitiga(7);

// soal 4
function ilove(n) {
  for (var i = 1; i <= n; i++) {
    switch (i % 3) {
      case 1:
        console.log(i + " - I love programming");
        break;
      case 2:
        console.log(i + " - I love Javascript");
        break;
      case 0:
        console.log(i + " - I love VueJS");
        console.log("===".repeat(i / 3));
        break;
    }
  }
  console.log();
}

ilove(3);
ilove(5);
ilove(7);
ilove(10);
