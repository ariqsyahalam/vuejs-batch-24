// soal 1
function jumlah_kata(str) {
  console.log(str.trim().split(" ").length);
}

var kalimat_1 = " Halo nama saya Muhammad Iqbal Mubarok ";
var kalimat_2 = "Saya Iqbal";

jumlah_kata(kalimat_1); // 6
jumlah_kata(kalimat_2); // 2

// soal 2
function next_date(tanggal, bulan, tahun) {
  if ([1, 3, 5, 7, 8, 10, 12].includes(bulan) && tanggal >= 31) {
    tanggal = 1;
    bulan += 1;
  } else if ([4, 6, 9, 11].includes(bulan) && tanggal >= 30) {
    console.log("OKK");
    tanggal = 1;
    bulan += 1;
  } else if (
    (tahun % 4 && tahun % 100 && tahun % 400) === 0 &&
    tanggal >= 28 &&
    bulan === 2
  ) {
    if (tanggal == 29) {
      tanggal = 1;
      bulan = 3;
    } else if (tanggal == 28) {
      tanggal = 29;
    }
  } else if ((tahun % 4 && tahun % 100 && tahun % 400) != 0 && tanggal >= 28) {
    tanggal = 1;
    bulan = 3;
  } else {
    tanggal += 1;
  }
  if (bulan > 12) {
    bulan = (bulan % 12) + 1;
    tahun += 1;
  }
  var bulanstr;
  switch (bulan) {
    case 1:
      bulanstr = "Januari";
      break;
    case 2:
      bulanstr = "Februari";
      break;
    case 3:
      bulanstr = "Maret";
      break;
    case 4:
      bulanstr = "April";
      break;
    case 5:
      bulanstr = "Mei";
      break;
    case 6:
      bulanstr = "Juni";
      break;
    case 7:
      bulanstr = "Juli";
      break;
    case 8:
      bulanstr = "Agustus";
      break;
    case 9:
      bulanstr = "September";
      break;
    case 10:
      bulanstr = "Oktober";
      break;
    case 11:
      bulanstr = "November";
      break;
    case 12:
      bulanstr = "Desember";
      break;
    default:
      break;
  }

  console.log(tanggal + " " + bulanstr + " " + tahun);
}

var tanggal = 29;
var bulan = 2;
var tahun = 2020;

next_date(tanggal, bulan, tahun); // output : 1 Maret 2020

var tanggal = 28;
var bulan = 2;
var tahun = 2021;

next_date(tanggal, bulan, tahun); // output : 1 Maret 2021

var tanggal = 31;
var bulan = 12;
var tahun = 2020;

next_date(tanggal, bulan, tahun); // output : 1 Januari 2021
